EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "AD9628"
Date "2020-02-01"
Rev "1"
Comp "Harmon Instruments"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L misc:testpoint_1mm TP?
U 1 1 5E8AFB1E
P 5900 1800
AR Path="/5BE20C19/5E8AFB1E" Ref="TP?"  Part="1" 
AR Path="/5E8AFB1E" Ref="TP2"  Part="1" 
F 0 "TP2" H 6050 1600 50  0000 L CNN
F 1 "testpoint_1mm" H 6100 1600 50  0001 L CNN
F 2 "kicad_pcb:TP_0.5mm" H 6100 1700 50  0001 L CNN
F 3 "" V 6150 1800 50  0001 C CNN
	1    5900 1800
	1    0    0    -1  
$EndComp
$Comp
L connector:HF9MW J1
U 1 1 5EC60C6F
P 6750 2800
F 0 "J1" H 6900 2873 50  0000 C CNN
F 1 "HF9MW" H 6750 2850 50  0001 L CNN
F 2 "kicad_pcb:fpc_0.3_45_socket" H 6750 2850 50  0001 L CNN
F 3 "$PARTS/HF9MW/ENG_CD_2328724_3.pdf" V 7000 2800 50  0001 C CNN
	1    6750 2800
	-1   0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 631BFAE8
P 7400 5500
F 0 "FB1" V 7163 5500 50  0000 C CNN
F 1 "fb0402" V 7254 5500 50  0000 C CNN
F 2 "kicad_pcb:L_0402_1005Metric" V 7330 5500 50  0001 C CNN
F 3 "~" H 7400 5500 50  0001 C CNN
	1    7400 5500
	0    -1   1    0   
$EndComp
Wire Wire Line
	6900 3000 6900 3100
Connection ~ 6900 3100
Wire Wire Line
	6900 3100 6900 3200
Connection ~ 6900 3200
Wire Wire Line
	6900 3200 6900 3300
Connection ~ 6900 3300
Wire Wire Line
	6900 3300 6900 3400
Connection ~ 6900 3400
Wire Wire Line
	6900 3400 6900 3500
Connection ~ 6900 3500
Wire Wire Line
	6900 3500 6900 3600
Connection ~ 6900 3600
Wire Wire Line
	6900 3600 6900 3700
Connection ~ 6900 3700
Wire Wire Line
	6900 3700 6900 3800
Connection ~ 6900 3800
Wire Wire Line
	6900 3800 6900 3900
Connection ~ 6900 3900
Wire Wire Line
	6900 3900 6900 4000
Connection ~ 6900 4000
Wire Wire Line
	6900 4000 6900 4100
Connection ~ 6900 4100
Wire Wire Line
	6900 4100 6900 4200
Connection ~ 6900 4200
Wire Wire Line
	6900 4200 6900 4300
Connection ~ 6900 4300
Wire Wire Line
	6900 4300 6900 4400
Connection ~ 6900 4400
Wire Wire Line
	6900 4400 6900 4500
Connection ~ 6900 4500
Wire Wire Line
	6900 4500 6900 4600
Connection ~ 6900 4600
Wire Wire Line
	6900 4600 6900 4700
Connection ~ 6900 4700
Wire Wire Line
	6900 4700 6900 4800
Connection ~ 6900 4800
Wire Wire Line
	6900 4800 6900 4900
Connection ~ 6900 4900
Wire Wire Line
	6900 4900 6900 5000
Wire Wire Line
	6900 5000 7000 5000
Wire Wire Line
	7000 5000 7000 5200
Wire Wire Line
	7000 5200 6900 5200
Connection ~ 6900 5000
Wire Wire Line
	7000 5200 7000 5300
Wire Wire Line
	7000 5300 6900 5300
Connection ~ 7000 5200
Wire Wire Line
	7000 5300 7000 5400
Connection ~ 7000 5300
$Comp
L combined:ground #PWR?
U 1 1 65AC1BF1
P 7000 5400
AR Path="/5BE20C19/65AC1BF1" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/65AC1BF1" Ref="#PWR?"  Part="1" 
AR Path="/65AC1BF1" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 7000 5400 50  0001 C CNN
F 1 "ground" H 7000 5330 50  0001 C CNN
F 2 "" H 7000 5400 50  0001 C CNN
F 3 "" H 7000 5400 50  0000 C CNN
	1    7000 5400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6300 2950 6200 2950
Wire Wire Line
	6200 2950 6200 2600
Wire Wire Line
	6950 2600 6950 2900
Wire Wire Line
	6950 2900 6900 2900
Wire Wire Line
	6900 5100 7150 5100
Wire Wire Line
	7150 5100 7150 5500
Wire Wire Line
	7150 5500 6250 5500
Wire Wire Line
	6250 5500 6250 5050
Wire Wire Line
	6250 5050 6300 5050
$Comp
L misc:tooling_hole TH3
U 1 1 5BBD5C94
P 750 7050
F 0 "TH3" H 850 7050 50  0000 L CNN
F 1 "tooling_hole" H 900 7050 50  0001 L CNN
F 2 "kicad_pcb:tooling_hole_2.55" H 900 7150 50  0001 L CNN
F 3 "" V 950 7250 50  0001 C CNN
	1    750  7050
	1    0    0    -1  
$EndComp
$Comp
L misc:tooling_hole TH4
U 1 1 5BBD5C9A
P 750 7200
F 0 "TH4" H 850 7200 50  0000 L CNN
F 1 "tooling_hole" H 900 7200 50  0001 L CNN
F 2 "kicad_pcb:tooling_hole_2.55" H 900 7300 50  0001 L CNN
F 3 "" V 950 7400 50  0001 C CNN
	1    750  7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 2550 10350 2600
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E40CC2E
P 9150 2800
AR Path="/5C2CDB08/5E40CC2E" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E40CC2E" Ref="C?"  Part="1" 
AR Path="/5E40CC2E" Ref="C1"  Part="1" 
F 0 "C1" V 9000 2800 50  0000 C CNN
F 1 "1 uF" V 9300 2800 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 9300 2700 50  0001 L CNN
F 3 "" H 9150 2800 50  0000 C CNN
	1    9150 2800
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5E40CC34
P 7500 4750
AR Path="/5C2CDB08/5E40CC34" Ref="#FLG?"  Part="1" 
AR Path="/5E374CB3/5E40CC34" Ref="#FLG?"  Part="1" 
AR Path="/5E40CC34" Ref="#FLG0102"  Part="1" 
F 0 "#FLG0102" H 7500 4825 50  0001 C CNN
F 1 "PWR_FLAG" V 7500 5050 50  0000 C CNN
F 2 "" H 7500 4750 50  0001 C CNN
F 3 "~" H 7500 4750 50  0001 C CNN
	1    7500 4750
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E40CC3A
P 7500 4750
AR Path="/5C2CDB08/5E40CC3A" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E40CC3A" Ref="#PWR?"  Part="1" 
AR Path="/5E40CC3A" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 7500 4750 50  0001 C CNN
F 1 "ground" H 7500 4680 50  0001 C CNN
F 2 "" H 7500 4750 50  0001 C CNN
F 3 "" H 7500 4750 50  0000 C CNN
	1    7500 4750
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E40CC40
P 10350 3050
AR Path="/5C2CDB08/5E40CC40" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E40CC40" Ref="#PWR?"  Part="1" 
AR Path="/5E40CC40" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 10350 3050 50  0001 C CNN
F 1 "ground" H 10350 2980 50  0001 C CNN
F 2 "" H 10350 3050 50  0001 C CNN
F 3 "" H 10350 3050 50  0000 C CNN
	1    10350 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 3000 10350 3050
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E40CC47
P 9550 2800
AR Path="/5C2CDB08/5E40CC47" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E40CC47" Ref="C?"  Part="1" 
AR Path="/5E40CC47" Ref="C2"  Part="1" 
F 0 "C2" V 9400 2800 50  0000 C CNN
F 1 "1 uF" V 9700 2800 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 9700 2700 50  0001 L CNN
F 3 "" H 9550 2800 50  0000 C CNN
	1    9550 2800
	1    0    0    -1  
$EndComp
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E40CC4D
P 10350 2800
AR Path="/5C2CDB08/5E40CC4D" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E40CC4D" Ref="C?"  Part="1" 
AR Path="/5E40CC4D" Ref="C3"  Part="1" 
F 0 "C3" V 10200 2800 50  0000 C CNN
F 1 "1 uF" V 10500 2800 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 10500 2700 50  0001 L CNN
F 3 "" H 10350 2800 50  0000 C CNN
	1    10350 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 2600 10350 2600
Wire Wire Line
	9950 3000 10350 3000
Connection ~ 10350 2600
Connection ~ 10350 3000
Wire Wire Line
	9150 2600 9550 2600
Wire Wire Line
	9150 3000 9550 3000
$Comp
L power:+3V8 #PWR0105
U 1 1 5E40CC5B
P 8100 2550
AR Path="/5E40CC5B" Ref="#PWR0105"  Part="1" 
AR Path="/5C2CDB08/5E40CC5B" Ref="#PWR?"  Part="1" 
F 0 "#PWR0105" H 8100 2400 50  0001 C CNN
F 1 "+3V8" H 8115 2723 50  0000 C CNN
F 2 "" H 8100 2550 50  0001 C CNN
F 3 "" H 8100 2550 50  0001 C CNN
	1    8100 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2550 8100 2600
Wire Wire Line
	8150 2600 8100 2600
Wire Wire Line
	9150 2600 8950 2600
Connection ~ 9150 2600
$Comp
L combined:ground #PWR?
U 1 1 5E40CC72
P 8550 3000
AR Path="/5C2CDB08/5E40CC72" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E40CC72" Ref="#PWR?"  Part="1" 
AR Path="/5E40CC72" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 8550 3000 50  0001 C CNN
F 1 "ground" H 8550 2930 50  0001 C CNN
F 2 "" H 8550 3000 50  0001 C CNN
F 3 "" H 8550 3000 50  0000 C CNN
	1    8550 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2600 8100 2800
Wire Wire Line
	8100 2800 8150 2800
Connection ~ 8100 2600
$Comp
L combined:ground #PWR?
U 1 1 5E4DA33C
P 4050 5200
AR Path="/5BE20C19/5E4DA33C" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E4DA33C" Ref="#PWR?"  Part="1" 
AR Path="/5E4DA33C" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 4050 5200 50  0001 C CNN
F 1 "ground" H 4050 5130 50  0001 C CNN
F 2 "" H 4050 5200 50  0001 C CNN
F 3 "" H 4050 5200 50  0000 C CNN
	1    4050 5200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4050 5200 4050 5150
$Comp
L power:+1V8 #PWR0107
U 1 1 5E4EE147
P 10350 2550
F 0 "#PWR0107" H 10350 2400 50  0001 C CNN
F 1 "+1V8" H 10365 2723 50  0000 C CNN
F 2 "" H 10350 2550 50  0001 C CNN
F 3 "" H 10350 2550 50  0001 C CNN
	1    10350 2550
	-1   0    0    -1  
$EndComp
$Comp
L resistor:resistor_0402 R?
U 1 1 5E59251C
P 3250 6100
AR Path="/5C2CDB08/5E59251C" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E59251C" Ref="R?"  Part="1" 
AR Path="/5E59251C" Ref="R10"  Part="1" 
F 0 "R10" V 3150 6100 50  0000 C CNN
F 1 "100" V 3250 6100 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 3150 5900 50  0001 C CNN
F 3 "" H 3250 6100 50  0000 C CNN
	1    3250 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 4950 3500 4950
Wire Wire Line
	3500 4950 3500 5850
Wire Wire Line
	3500 5850 3350 5850
Wire Wire Line
	4050 5050 3550 5050
Wire Wire Line
	3550 5050 3550 6350
Wire Wire Line
	3550 6350 3350 6350
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E59E4B2
P 2900 6100
AR Path="/5C2CDB08/5E59E4B2" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E59E4B2" Ref="C?"  Part="1" 
AR Path="/5E59E4B2" Ref="C13"  Part="1" 
F 0 "C13" V 2750 6100 50  0000 C CNN
F 1 "33 pF" V 3050 6100 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 3050 6000 50  0001 L CNN
F 3 "" H 2900 6100 50  0000 C CNN
	1    2900 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 5850 2900 5850
Wire Wire Line
	2900 5850 2900 5900
Connection ~ 3250 5850
Wire Wire Line
	3250 6350 2900 6350
Wire Wire Line
	2900 6350 2900 6300
Connection ~ 3250 6350
$Comp
L resistor:resistor_0402 R?
U 1 1 5E5C74FD
P 2250 5850
AR Path="/5C2CDB08/5E5C74FD" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E5C74FD" Ref="R?"  Part="1" 
AR Path="/5E5C74FD" Ref="R8"  Part="1" 
F 0 "R8" V 2150 5850 50  0000 C CNN
F 1 "49.9" V 2250 5850 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 2150 5650 50  0001 C CNN
F 3 "" H 2250 5850 50  0000 C CNN
	1    2250 5850
	1    0    0    1   
$EndComp
$Comp
L resistor:resistor_0402 R?
U 1 1 5E5C7507
P 2250 6350
AR Path="/5C2CDB08/5E5C7507" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5E5C7507" Ref="R?"  Part="1" 
AR Path="/5E5C7507" Ref="R9"  Part="1" 
F 0 "R9" V 2150 6350 50  0000 C CNN
F 1 "49.9" V 2250 6350 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 2150 6150 50  0001 C CNN
F 3 "" H 2250 6350 50  0000 C CNN
	1    2250 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 5600 2400 5600
Wire Wire Line
	2900 5600 2900 5850
Connection ~ 2900 5850
Wire Wire Line
	2250 6600 2400 6600
Wire Wire Line
	2900 6600 2900 6350
Connection ~ 2900 6350
$Comp
L combined:ground #PWR?
U 1 1 5E5E0421
P 2200 6100
AR Path="/5BE20C19/5E5E0421" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E5E0421" Ref="#PWR?"  Part="1" 
AR Path="/5E5E0421" Ref="#PWR0122"  Part="1" 
F 0 "#PWR0122" H 2200 6100 50  0001 C CNN
F 1 "ground" H 2200 6030 50  0001 C CNN
F 2 "" H 2200 6100 50  0001 C CNN
F 3 "" H 2200 6100 50  0000 C CNN
	1    2200 6100
	0    1    -1   0   
$EndComp
$Comp
L misc:CT6XQ T2
U 1 1 5E5ED7FF
P 1650 6100
F 0 "T2" H 1650 6381 50  0000 C CNN
F 1 "CT6XQ" H 1650 6290 50  0000 C CNN
F 2 "kicad_pcb:Macom_SM-22" H 1650 5950 50  0001 C CNN
F 3 "$PARTS/CT6XQ/MABA-007159-000000-13414.pdf" V 1650 6100 50  0001 C CNN
	1    1650 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6600 2050 6600
Wire Wire Line
	2050 6600 2050 6200
Connection ~ 2250 6600
Wire Wire Line
	2050 6000 2050 5600
Wire Wire Line
	2050 5600 2250 5600
Connection ~ 2250 5600
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E60AC0D
P 2600 6600
AR Path="/5C2CDB08/5E60AC0D" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E60AC0D" Ref="C?"  Part="1" 
AR Path="/5E60AC0D" Ref="C10"  Part="1" 
F 0 "C10" V 2450 6600 50  0000 C CNN
F 1 "1 uF" V 2750 6600 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 2750 6500 50  0001 L CNN
F 3 "" H 2600 6600 50  0000 C CNN
	1    2600 6600
	0    1    1    0   
$EndComp
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E60B4AB
P 2600 5600
AR Path="/5C2CDB08/5E60B4AB" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E60B4AB" Ref="C?"  Part="1" 
AR Path="/5E60B4AB" Ref="C11"  Part="1" 
F 0 "C11" V 2450 5600 50  0000 C CNN
F 1 "1 uF" V 2750 5600 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 2750 5500 50  0001 L CNN
F 3 "" H 2600 5600 50  0000 C CNN
	1    2600 5600
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 6200 1050 6200
$Comp
L combined:ground #PWR?
U 1 1 5E61DC9C
P 1200 6050
AR Path="/5BE20C19/5E61DC9C" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E61DC9C" Ref="#PWR?"  Part="1" 
AR Path="/5E61DC9C" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 1200 6050 50  0001 C CNN
F 1 "ground" H 1200 5980 50  0001 C CNN
F 2 "" H 1200 6050 50  0001 C CNN
F 3 "" H 1200 6050 50  0000 C CNN
	1    1200 6050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1250 6000 1200 6000
Wire Wire Line
	1200 6000 1200 6050
$Comp
L combined:ground #PWR?
U 1 1 5E629F1E
P 900 6400
AR Path="/5BE20C19/5E629F1E" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5E629F1E" Ref="#PWR?"  Part="1" 
AR Path="/5E629F1E" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 900 6400 50  0001 C CNN
F 1 "ground" H 900 6330 50  0001 C CNN
F 2 "" H 900 6400 50  0001 C CNN
F 3 "" H 900 6400 50  0000 C CNN
	1    900  6400
	-1   0    0    -1  
$EndComp
Text Label 5800 1900 2    50   ~ 0
CLKB
Wire Wire Line
	5800 1900 5500 1900
Text Label 5800 2000 2    50   ~ 0
CLKA
Wire Wire Line
	5800 2000 5500 2000
Text Label 5250 2850 2    50   ~ 0
D0
Wire Wire Line
	5250 2850 4950 2850
Text Label 5250 2950 2    50   ~ 0
D1
Wire Wire Line
	5250 2950 4950 2950
Text Label 5250 3050 2    50   ~ 0
D2
Wire Wire Line
	5250 3050 4950 3050
Text Label 5250 3150 2    50   ~ 0
D3
Wire Wire Line
	5250 3150 4950 3150
Text Label 5250 3250 2    50   ~ 0
D4
Wire Wire Line
	5250 3250 4950 3250
Text Label 5250 3350 2    50   ~ 0
D5
Wire Wire Line
	5250 3350 4950 3350
Text Label 5250 3450 2    50   ~ 0
D6
Wire Wire Line
	5250 3450 4950 3450
Text Label 5250 3550 2    50   ~ 0
D7
Wire Wire Line
	5250 3550 4950 3550
Text Label 5250 3650 2    50   ~ 0
D8
Wire Wire Line
	5250 3650 4950 3650
Text Label 5250 3750 2    50   ~ 0
D9
Wire Wire Line
	5250 3750 4950 3750
Text Label 5250 3850 2    50   ~ 0
D10
Wire Wire Line
	5250 3850 4950 3850
Text Label 5250 3950 2    50   ~ 0
D11
Wire Wire Line
	5250 3950 4950 3950
Text Label 5250 4150 2    50   ~ 0
SDIO
Wire Wire Line
	5250 4150 4950 4150
Text Label 5250 4250 2    50   ~ 0
SCLK
Wire Wire Line
	5250 4250 4950 4250
Text Label 5250 4350 2    50   ~ 0
~CS
Wire Wire Line
	5250 4350 4950 4350
Text Label 6000 4250 0    50   ~ 0
CLKA
Wire Wire Line
	6000 4250 6300 4250
Text Label 6000 3050 0    50   ~ 0
D0
Wire Wire Line
	6000 3050 6300 3050
Text Label 6000 3150 0    50   ~ 0
D1
Wire Wire Line
	6000 3150 6300 3150
Text Label 6000 3250 0    50   ~ 0
D2
Wire Wire Line
	6000 3250 6300 3250
Text Label 6000 3350 0    50   ~ 0
D3
Wire Wire Line
	6000 3350 6300 3350
Text Label 6000 3450 0    50   ~ 0
D4
Wire Wire Line
	6000 3450 6300 3450
Text Label 6000 3550 0    50   ~ 0
D5
Wire Wire Line
	6000 3550 6300 3550
Text Label 6000 3650 0    50   ~ 0
D6
Wire Wire Line
	6000 3650 6300 3650
Text Label 6000 3750 0    50   ~ 0
D7
Wire Wire Line
	6000 3750 6300 3750
Text Label 6000 3850 0    50   ~ 0
D8
Wire Wire Line
	6000 3850 6300 3850
Text Label 6000 3950 0    50   ~ 0
D9
Wire Wire Line
	6000 3950 6300 3950
Text Label 6000 4050 0    50   ~ 0
D10
Wire Wire Line
	6000 4050 6300 4050
Text Label 6000 4150 0    50   ~ 0
D11
Wire Wire Line
	6000 4150 6300 4150
Text Label 5700 2850 0    50   ~ 0
OF
Wire Wire Line
	5700 2850 6000 2850
$Comp
L misc:testpoint_1mm TP?
U 1 1 5E851703
P 5350 2650
AR Path="/5BE20C19/5E851703" Ref="TP?"  Part="1" 
AR Path="/5E851703" Ref="TP1"  Part="1" 
F 0 "TP1" H 5500 2450 50  0000 L CNN
F 1 "testpoint_1mm" H 5550 2450 50  0001 L CNN
F 2 "kicad_pcb:TP_0.5mm" H 5550 2550 50  0001 L CNN
F 3 "" V 5600 2650 50  0001 C CNN
	1    5350 2650
	1    0    0    -1  
$EndComp
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E87301C
P 7800 5700
AR Path="/5C2CDB08/5E87301C" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E87301C" Ref="C?"  Part="1" 
AR Path="/5E87301C" Ref="C6"  Part="1" 
F 0 "C6" V 7650 5700 50  0000 C CNN
F 1 "1 uF" V 7950 5700 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 7950 5600 50  0001 L CNN
F 3 "" H 7800 5700 50  0000 C CNN
	1    7800 5700
	1    0    0    -1  
$EndComp
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5E873026
P 8200 5700
AR Path="/5C2CDB08/5E873026" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5E873026" Ref="C?"  Part="1" 
AR Path="/5E873026" Ref="C14"  Part="1" 
F 0 "C14" V 8050 5700 50  0000 C CNN
F 1 "1 uF" V 8350 5700 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 8350 5600 50  0001 L CNN
F 3 "" H 8200 5700 50  0000 C CNN
	1    8200 5700
	1    0    0    -1  
$EndComp
Connection ~ 7800 5500
Wire Wire Line
	7800 5500 8200 5500
Wire Wire Line
	7800 5900 8200 5900
Wire Wire Line
	8200 5950 8200 5900
Connection ~ 8200 5900
$Comp
L combined:ground #PWR?
U 1 1 5E8A8EA7
P 8200 5950
AR Path="/5C2CDB08/5E8A8EA7" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E8A8EA7" Ref="#PWR?"  Part="1" 
AR Path="/5E8A8EA7" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 8200 5950 50  0001 C CNN
F 1 "ground" H 8200 5880 50  0001 C CNN
F 2 "" H 8200 5950 50  0001 C CNN
F 3 "" H 8200 5950 50  0000 C CNN
	1    8200 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 5500 7650 5500
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5E8EA35A
P 7650 5500
AR Path="/5C2CDB08/5E8EA35A" Ref="#FLG?"  Part="1" 
AR Path="/5E374CB3/5E8EA35A" Ref="#FLG?"  Part="1" 
AR Path="/5E8EA35A" Ref="#FLG0101"  Part="1" 
F 0 "#FLG0101" H 7650 5575 50  0001 C CNN
F 1 "PWR_FLAG" V 7650 5800 50  0000 C CNN
F 2 "" H 7650 5500 50  0001 C CNN
F 3 "~" H 7650 5500 50  0001 C CNN
	1    7650 5500
	1    0    0    -1  
$EndComp
Connection ~ 7650 5500
Wire Wire Line
	7650 5500 7800 5500
Wire Wire Line
	7300 5500 7150 5500
Connection ~ 7150 5500
Text Label 6600 5500 0    50   ~ 0
1V8I
NoConn ~ 5500 1900
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5E93B301
P 7850 2600
AR Path="/5C2CDB08/5E93B301" Ref="#FLG?"  Part="1" 
AR Path="/5E374CB3/5E93B301" Ref="#FLG?"  Part="1" 
AR Path="/5E93B301" Ref="#FLG0103"  Part="1" 
F 0 "#FLG0103" H 7850 2675 50  0001 C CNN
F 1 "PWR_FLAG" V 7850 2900 50  0000 C CNN
F 2 "" H 7850 2600 50  0001 C CNN
F 3 "~" H 7850 2600 50  0001 C CNN
	1    7850 2600
	1    0    0    -1  
$EndComp
$Comp
L capacitors:capacitor_0603 C15
U 1 1 5E998044
P 7750 2800
F 0 "C15" H 7885 2846 50  0000 L CNN
F 1 "2.2 uF" H 7885 2755 50  0000 L CNN
F 2 "kicad_pcb:C_0603_1608Metric" H 7900 2700 50  0001 L CNN
F 3 "" H 7750 2800 50  0000 C CNN
	1    7750 2800
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5E998B4A
P 7750 3000
AR Path="/5C2CDB08/5E998B4A" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5E998B4A" Ref="#PWR?"  Part="1" 
AR Path="/5E998B4A" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 7750 3000 50  0001 C CNN
F 1 "ground" H 7750 2930 50  0001 C CNN
F 2 "" H 7750 3000 50  0001 C CNN
F 3 "" H 7750 3000 50  0000 C CNN
	1    7750 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2600 7850 2600
Wire Wire Line
	6200 2600 6950 2600
Connection ~ 6950 2600
Connection ~ 7750 2600
Wire Wire Line
	7750 2600 7700 2600
Connection ~ 7850 2600
Wire Wire Line
	7850 2600 7750 2600
Text Label 6600 2600 0    50   ~ 0
3V8I
Wire Wire Line
	2250 6100 2200 6100
Connection ~ 2250 6100
Wire Wire Line
	2800 5600 2900 5600
Wire Wire Line
	2800 6600 2900 6600
Wire Wire Line
	3350 5950 3350 5850
Connection ~ 3350 5850
Wire Wire Line
	3350 5850 3250 5850
Wire Wire Line
	3350 6250 3350 6350
Connection ~ 3350 6350
Wire Wire Line
	3350 6350 3250 6350
$Comp
L Device:L L1
U 1 1 5EC1ADC1
P 3350 6100
F 0 "L1" H 3402 6146 50  0000 L CNN
F 1 "47 nH" H 3402 6055 50  0000 L CNN
F 2 "kicad_pcb:L_0402_1005Metric" H 3350 6100 50  0001 C CNN
F 3 "~" H 3350 6100 50  0001 C CNN
	1    3350 6100
	1    0    0    -1  
$EndComp
$Comp
L misc:6H67G Fl1
U 1 1 5EC6399D
P 7350 2600
F 0 "Fl1" H 7350 2723 50  0000 C CNN
F 1 "6H67G" H 7150 2700 50  0001 L BNN
F 2 "kicad_pcb:NFE31" H 7350 2600 50  0001 L BNN
F 3 "$PARTS/6H67G/NFE31PT222Z1E9#.pdf" H 7350 2600 50  0001 L BNN
	1    7350 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2600 6950 2600
$Comp
L combined:ground #PWR?
U 1 1 5EC9F231
P 7350 2800
AR Path="/5C2CDB08/5EC9F231" Ref="#PWR?"  Part="1" 
AR Path="/5E374CB3/5EC9F231" Ref="#PWR?"  Part="1" 
AR Path="/5EC9F231" Ref="#PWR0128"  Part="1" 
F 0 "#PWR0128" H 7350 2800 50  0001 C CNN
F 1 "ground" H 7350 2730 50  0001 C CNN
F 2 "" H 7350 2800 50  0001 C CNN
F 3 "" H 7350 2800 50  0000 C CNN
	1    7350 2800
	1    0    0    -1  
$EndComp
$Comp
L ic:VRVC9 U2
U 1 1 5ECBF376
P 4200 2750
F 0 "U2" H 4500 2915 50  0000 C CNN
F 1 "VRVC9" H 4500 2824 50  0000 C CNN
F 2 "kicad_pcb:QFN-64-1EP_9x9mm_P0.5mm_EP6.2x6.2mm" H 4500 2800 50  0001 L CNN
F 3 "$PARTS/VRVC9/AD9628.pdf" V 4450 2750 50  0001 C CNN
	1    4200 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2800 4000 2850
Wire Wire Line
	4000 3150 4050 3150
Wire Wire Line
	4050 3050 4000 3050
Connection ~ 4000 3050
Wire Wire Line
	4000 3050 4000 3150
Wire Wire Line
	4050 2950 4000 2950
Connection ~ 4000 2950
Wire Wire Line
	4000 2950 4000 3050
Wire Wire Line
	4050 2850 4000 2850
Connection ~ 4000 2850
Wire Wire Line
	4000 2850 4000 2950
$Comp
L combined:ground #PWR?
U 1 1 5ED42F73
P 5000 4700
AR Path="/5BE20C19/5ED42F73" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5ED42F73" Ref="#PWR?"  Part="1" 
AR Path="/5ED42F73" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 5000 4700 50  0001 C CNN
F 1 "ground" H 5000 4630 50  0001 C CNN
F 2 "" H 5000 4700 50  0001 C CNN
F 3 "" H 5000 4700 50  0000 C CNN
	1    5000 4700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5000 4700 5000 4650
Wire Wire Line
	5000 4450 4950 4450
Wire Wire Line
	4950 4550 5000 4550
Connection ~ 5000 4550
Wire Wire Line
	5000 4550 5000 4450
Wire Wire Line
	4950 4650 5000 4650
Connection ~ 5000 4650
Wire Wire Line
	5000 4650 5000 4550
Wire Wire Line
	4050 3250 4050 3350
Connection ~ 4050 3350
Wire Wire Line
	4050 3350 4050 3450
Connection ~ 4050 3450
Wire Wire Line
	4050 3450 4050 3550
Connection ~ 4050 3550
Wire Wire Line
	4050 3550 4050 3650
Connection ~ 4050 3650
Wire Wire Line
	4050 3650 4050 3750
Connection ~ 4050 3750
Wire Wire Line
	4050 3750 4050 3850
Connection ~ 4050 3850
Wire Wire Line
	4050 3850 4050 3950
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5EDB2BE6
P 5500 5100
AR Path="/5C2CDB08/5EDB2BE6" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5EDB2BE6" Ref="C?"  Part="1" 
AR Path="/5EDB2BE6" Ref="C17"  Part="1" 
F 0 "C17" V 5350 5100 50  0000 C CNN
F 1 "1 uF" V 5650 5100 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 5650 5000 50  0001 L CNN
F 3 "" H 5500 5100 50  0000 C CNN
	1    5500 5100
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5EDB38C7
P 5500 5300
AR Path="/5BE20C19/5EDB38C7" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5EDB38C7" Ref="#PWR?"  Part="1" 
AR Path="/5EDB38C7" Ref="#PWR0125"  Part="1" 
F 0 "#PWR0125" H 5500 5300 50  0001 C CNN
F 1 "ground" H 5500 5230 50  0001 C CNN
F 2 "" H 5500 5300 50  0001 C CNN
F 3 "" H 5500 5300 50  0000 C CNN
	1    5500 5300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4950 4850 5500 4850
Wire Wire Line
	5500 4850 5500 4900
Text Label 5250 4850 2    50   ~ 0
VREF
$Comp
L combined:ground #PWR?
U 1 1 5EDBD9C0
P 4950 4950
AR Path="/5BE20C19/5EDBD9C0" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5EDBD9C0" Ref="#PWR?"  Part="1" 
AR Path="/5EDBD9C0" Ref="#PWR0126"  Part="1" 
F 0 "#PWR0126" H 4950 4950 50  0001 C CNN
F 1 "ground" H 4950 4880 50  0001 C CNN
F 2 "" H 4950 4950 50  0001 C CNN
F 3 "" H 4950 4950 50  0000 C CNN
	1    4950 4950
	0    -1   1    0   
$EndComp
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5EDBE66B
P 5200 5700
AR Path="/5C2CDB08/5EDBE66B" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5EDBE66B" Ref="C?"  Part="1" 
AR Path="/5EDBE66B" Ref="C12"  Part="1" 
F 0 "C12" V 5050 5700 50  0000 C CNN
F 1 "100 nF" V 5350 5700 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 5350 5600 50  0001 L CNN
F 3 "" H 5200 5700 50  0000 C CNN
	1    5200 5700
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5EDBEC4D
P 5200 5900
AR Path="/5BE20C19/5EDBEC4D" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5EDBEC4D" Ref="#PWR?"  Part="1" 
AR Path="/5EDBEC4D" Ref="#PWR0127"  Part="1" 
F 0 "#PWR0127" H 5200 5900 50  0001 C CNN
F 1 "ground" H 5200 5830 50  0001 C CNN
F 2 "" H 5200 5900 50  0001 C CNN
F 3 "" H 5200 5900 50  0000 C CNN
	1    5200 5900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4950 5050 5200 5050
Wire Wire Line
	5200 5050 5200 5450
$Comp
L connector:6BPPF P1
U 1 1 5EDCC993
P 900 6200
F 0 "P1" H 854 6347 50  0000 C CNN
F 1 "6BPPF" H 900 6350 50  0001 C CNN
F 2 "kicad_pcb:U.FL_Molex_MCRF_73412-0110_Vertical" H 900 6350 50  0001 C CNN
F 3 "$PARTS/6BPPF/A-1JB.pdf" H 900 6200 50  0001 C CNN
	1    900  6200
	-1   0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5EDDB821
P 3050 5750
AR Path="/5BE20C19/5EDDB821" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5EDDB821" Ref="#PWR?"  Part="1" 
AR Path="/5EDDB821" Ref="#PWR0129"  Part="1" 
F 0 "#PWR0129" H 3050 5750 50  0001 C CNN
F 1 "ground" H 3050 5680 50  0001 C CNN
F 2 "" H 3050 5750 50  0001 C CNN
F 3 "" H 3050 5750 50  0000 C CNN
	1    3050 5750
	-1   0    0    -1  
$EndComp
$Comp
L connector:6BPPF P2
U 1 1 5EDDB82B
P 3050 5550
F 0 "P2" H 3004 5697 50  0000 C CNN
F 1 "6BPPF" H 3050 5700 50  0001 C CNN
F 2 "kicad_pcb:U.FL_Molex_MCRF_73412-0110_Vertical" H 3050 5700 50  0001 C CNN
F 3 "$PARTS/6BPPF/A-1JB.pdf" H 3050 5550 50  0001 C CNN
	1    3050 5550
	-1   0    0    -1  
$EndComp
$Comp
L resistor:resistor_0402 R?
U 1 1 5EE0EA63
P 4950 5550
AR Path="/5C2CDB08/5EE0EA63" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5EE0EA63" Ref="R?"  Part="1" 
AR Path="/5EE0EA63" Ref="R2"  Part="1" 
F 0 "R2" V 4850 5550 50  0000 C CNN
F 1 "10 k" V 4950 5550 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 4850 5350 50  0001 C CNN
F 3 "" H 4950 5550 50  0000 C CNN
	1    4950 5550
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5EE0F41B
P 4950 5800
AR Path="/5BE20C19/5EE0F41B" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5EE0F41B" Ref="#PWR?"  Part="1" 
AR Path="/5EE0F41B" Ref="#PWR0130"  Part="1" 
F 0 "#PWR0130" H 4950 5800 50  0001 C CNN
F 1 "ground" H 4950 5730 50  0001 C CNN
F 2 "" H 4950 5800 50  0001 C CNN
F 3 "" H 4950 5800 50  0000 C CNN
	1    4950 5800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4950 5300 4950 5150
Text Label 5200 5050 2    50   ~ 0
VCM
Text Label 4950 5300 1    50   ~ 0
RB
$Comp
L ic:VQ8V4 U1
U 1 1 5EE1E27F
P 8250 2500
F 0 "U1" H 8550 2665 50  0000 C CNN
F 1 "VQ8V4" H 8550 2574 50  0000 C CNN
F 2 "kicad_pcb:SOT-23-5" H 8550 2550 50  0001 L CNN
F 3 "$PARTS/CGK8Y/lp5907.pdf" V 8500 2500 50  0001 C CNN
	1    8250 2500
	1    0    0    -1  
$EndComp
NoConn ~ 8950 2800
Text Label 6000 4550 0    50   ~ 0
~CS
Wire Wire Line
	6000 4550 6300 4550
Text Label 6000 4450 0    50   ~ 0
SCLK
Wire Wire Line
	6000 4450 6300 4450
Text Label 6000 4350 0    50   ~ 0
SDIO
Wire Wire Line
	6000 4350 6300 4350
Text Label 7950 5500 0    50   ~ 0
1V8D
Text Label 4000 2800 1    50   ~ 0
1V8D
$Comp
L power:+1V8 #PWR0108
U 1 1 5EE9A418
P 4050 3250
F 0 "#PWR0108" H 4050 3100 50  0001 C CNN
F 1 "+1V8" H 4065 3423 50  0000 C CNN
F 2 "" H 4050 3250 50  0001 C CNN
F 3 "" H 4050 3250 50  0001 C CNN
	1    4050 3250
	0    -1   1    0   
$EndComp
Connection ~ 4050 3250
$Comp
L combined:ground #PWR?
U 1 1 5EEF1AF3
P 2850 3450
AR Path="/5BE20C19/5EEF1AF3" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5EEF1AF3" Ref="#PWR?"  Part="1" 
AR Path="/5EEF1AF3" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 2850 3450 50  0001 C CNN
F 1 "ground" H 2850 3380 50  0001 C CNN
F 2 "" H 2850 3450 50  0001 C CNN
F 3 "" H 2850 3450 50  0000 C CNN
	1    2850 3450
	-1   0    0    -1  
$EndComp
$Comp
L connector:6BPPF P3
U 1 1 5EEF1AFD
P 2850 3250
F 0 "P3" H 2804 3397 50  0000 C CNN
F 1 "6BPPF" H 2850 3400 50  0001 C CNN
F 2 "kicad_pcb:U.FL_Molex_MCRF_73412-0110_Vertical" H 2850 3400 50  0001 C CNN
F 3 "$PARTS/6BPPF/A-1JB.pdf" H 2850 3250 50  0001 C CNN
	1    2850 3250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4050 4150 3300 4150
Wire Wire Line
	3300 4150 3300 3250
Wire Wire Line
	3300 3250 3150 3250
$Comp
L combined:ground #PWR?
U 1 1 5EF05FED
P 2850 4450
AR Path="/5BE20C19/5EF05FED" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5EF05FED" Ref="#PWR?"  Part="1" 
AR Path="/5EF05FED" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 2850 4450 50  0001 C CNN
F 1 "ground" H 2850 4380 50  0001 C CNN
F 2 "" H 2850 4450 50  0001 C CNN
F 3 "" H 2850 4450 50  0000 C CNN
	1    2850 4450
	-1   0    0    -1  
$EndComp
$Comp
L connector:6BPPF P4
U 1 1 5EF05FF7
P 2850 4250
F 0 "P4" H 2804 4397 50  0000 C CNN
F 1 "6BPPF" H 2850 4400 50  0001 C CNN
F 2 "kicad_pcb:U.FL_Molex_MCRF_73412-0110_Vertical" H 2850 4400 50  0001 C CNN
F 3 "$PARTS/6BPPF/A-1JB.pdf" H 2850 4250 50  0001 C CNN
	1    2850 4250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4050 4250 3150 4250
$Comp
L combined:ground #PWR?
U 1 1 5EF19B34
P 3050 4750
AR Path="/5BE20C19/5EF19B34" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5EF19B34" Ref="#PWR?"  Part="1" 
AR Path="/5EF19B34" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 3050 4750 50  0001 C CNN
F 1 "ground" H 3050 4680 50  0001 C CNN
F 2 "" H 3050 4750 50  0001 C CNN
F 3 "" H 3050 4750 50  0000 C CNN
	1    3050 4750
	-1   0    0    -1  
$EndComp
$Comp
L connector:6BPPF P5
U 1 1 5EF19B3E
P 3050 4550
F 0 "P5" H 3004 4697 50  0000 C CNN
F 1 "6BPPF" H 3050 4700 50  0001 C CNN
F 2 "kicad_pcb:U.FL_Molex_MCRF_73412-0110_Vertical" H 3050 4700 50  0001 C CNN
F 3 "$PARTS/6BPPF/A-1JB.pdf" H 3050 4550 50  0001 C CNN
	1    3050 4550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4050 4550 3300 4550
Wire Wire Line
	3400 4650 3400 5550
Wire Wire Line
	3400 5550 3300 5550
Wire Wire Line
	3400 4650 4050 4650
$Comp
L resistor:resistor_0402 R?
U 1 1 5EF7C61E
P 3300 4800
AR Path="/5C2CDB08/5EF7C61E" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5EF7C61E" Ref="R?"  Part="1" 
AR Path="/5EF7C61E" Ref="R4"  Part="1" 
F 0 "R4" V 3200 4800 50  0000 C CNN
F 1 "49.9" V 3300 4800 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 3200 4600 50  0001 C CNN
F 3 "" H 3300 4800 50  0000 C CNN
	1    3300 4800
	1    0    0    1   
$EndComp
Connection ~ 3300 4550
Wire Wire Line
	3300 4550 3200 4550
$Comp
L resistor:resistor_0402 R?
U 1 1 5EF88203
P 3300 5300
AR Path="/5C2CDB08/5EF88203" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5EF88203" Ref="R?"  Part="1" 
AR Path="/5EF88203" Ref="R5"  Part="1" 
F 0 "R5" V 3200 5300 50  0000 C CNN
F 1 "49.9" V 3300 5300 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 3200 5100 50  0001 C CNN
F 3 "" H 3300 5300 50  0000 C CNN
	1    3300 5300
	1    0    0    1   
$EndComp
Connection ~ 3300 5550
Wire Wire Line
	3300 5550 3200 5550
$Comp
L resistor:resistor_0402 R?
U 1 1 5EFAE37E
P 3150 3500
AR Path="/5C2CDB08/5EFAE37E" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5EFAE37E" Ref="R?"  Part="1" 
AR Path="/5EFAE37E" Ref="R1"  Part="1" 
F 0 "R1" V 3050 3500 50  0000 C CNN
F 1 "49.9" V 3150 3500 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 3050 3300 50  0001 C CNN
F 3 "" H 3150 3500 50  0000 C CNN
	1    3150 3500
	1    0    0    1   
$EndComp
$Comp
L resistor:resistor_0402 R?
U 1 1 5EFAE388
P 3150 4000
AR Path="/5C2CDB08/5EFAE388" Ref="R?"  Part="1" 
AR Path="/5E374CB3/5EFAE388" Ref="R?"  Part="1" 
AR Path="/5EFAE388" Ref="R3"  Part="1" 
F 0 "R3" V 3050 4000 50  0000 C CNN
F 1 "49.9" V 3150 4000 50  0000 C CNN
F 2 "kicad_pcb:R_0402_1005Metric" V 3050 3800 50  0001 C CNN
F 3 "" H 3150 4000 50  0000 C CNN
	1    3150 4000
	1    0    0    1   
$EndComp
Connection ~ 3150 4250
Wire Wire Line
	3150 4250 3000 4250
Connection ~ 3150 3250
Wire Wire Line
	3150 3250 3000 3250
Text Label 2850 3750 0    50   ~ 0
VCM
Text Label 3000 5050 0    50   ~ 0
VCM
Wire Wire Line
	2850 3750 3150 3750
Connection ~ 3150 3750
Wire Wire Line
	3000 5050 3300 5050
Connection ~ 3300 5050
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5EFFA232
P 1900 4800
AR Path="/5C2CDB08/5EFFA232" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5EFFA232" Ref="C?"  Part="1" 
AR Path="/5EFFA232" Ref="C4"  Part="1" 
F 0 "C4" V 1750 4800 50  0000 C CNN
F 1 "100 nF" V 2050 4800 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 2050 4700 50  0001 L CNN
F 3 "" H 1900 4800 50  0000 C CNN
	1    1900 4800
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5EFFC0CC
P 1900 5000
AR Path="/5BE20C19/5EFFC0CC" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5EFFC0CC" Ref="#PWR?"  Part="1" 
AR Path="/5EFFC0CC" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 1900 5000 50  0001 C CNN
F 1 "ground" H 1900 4930 50  0001 C CNN
F 2 "" H 1900 5000 50  0001 C CNN
F 3 "" H 1900 5000 50  0000 C CNN
	1    1900 5000
	-1   0    0    -1  
$EndComp
Text Label 1700 4600 0    50   ~ 0
VCM
Wire Wire Line
	1700 4600 1900 4600
Connection ~ 1900 4600
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5F0084B8
P 2300 4800
AR Path="/5C2CDB08/5F0084B8" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5F0084B8" Ref="C?"  Part="1" 
AR Path="/5F0084B8" Ref="C5"  Part="1" 
F 0 "C5" V 2150 4800 50  0000 C CNN
F 1 "100 nF" V 2450 4800 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 2450 4700 50  0001 L CNN
F 3 "" H 2300 4800 50  0000 C CNN
	1    2300 4800
	1    0    0    -1  
$EndComp
$Comp
L combined:ground #PWR?
U 1 1 5F0084C2
P 2300 5000
AR Path="/5BE20C19/5F0084C2" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5F0084C2" Ref="#PWR?"  Part="1" 
AR Path="/5F0084C2" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 2300 5000 50  0001 C CNN
F 1 "ground" H 2300 4930 50  0001 C CNN
F 2 "" H 2300 5000 50  0001 C CNN
F 3 "" H 2300 5000 50  0000 C CNN
	1    2300 5000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 4600 2300 4600
Text Label 5250 4050 2    50   ~ 0
CLKA
Wire Wire Line
	5250 4050 4950 4050
$Comp
L capacitors:capacitor_0402 C?
U 1 1 5F12C5CF
P 9950 2800
AR Path="/5C2CDB08/5F12C5CF" Ref="C?"  Part="1" 
AR Path="/5E374CB3/5F12C5CF" Ref="C?"  Part="1" 
AR Path="/5F12C5CF" Ref="C7"  Part="1" 
F 0 "C7" V 9800 2800 50  0000 C CNN
F 1 "1 uF" V 10100 2800 50  0000 C CNN
F 2 "kicad_pcb:C_0402_1005Metric" H 10100 2700 50  0001 L CNN
F 3 "" H 9950 2800 50  0000 C CNN
	1    9950 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 2600 9950 2600
Wire Wire Line
	9550 3000 9950 3000
Connection ~ 9550 2600
Connection ~ 9550 3000
Connection ~ 9950 2600
Connection ~ 9950 3000
$Comp
L combined:ground #PWR?
U 1 1 5F14B68D
P 6050 5650
AR Path="/5BE20C19/5F14B68D" Ref="#PWR?"  Part="1" 
AR Path="/5CDB68FA/5F14B68D" Ref="#PWR?"  Part="1" 
AR Path="/5F14B68D" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 6050 5650 50  0001 C CNN
F 1 "ground" H 6050 5580 50  0001 C CNN
F 2 "" H 6050 5650 50  0001 C CNN
F 3 "" H 6050 5650 50  0000 C CNN
	1    6050 5650
	1    0    0    -1  
$EndComp
$Comp
L connector:6BPPF P6
U 1 1 5F14B697
P 6050 5450
F 0 "P6" H 6004 5597 50  0000 C CNN
F 1 "6BPPF" H 6050 5600 50  0001 C CNN
F 2 "kicad_pcb:U.FL_Molex_MCRF_73412-0110_Vertical" H 6050 5600 50  0001 C CNN
F 3 "$PARTS/6BPPF/A-1JB.pdf" H 6050 5450 50  0001 C CNN
	1    6050 5450
	1    0    0    -1  
$EndComp
Connection ~ 5200 5450
Wire Wire Line
	5200 5450 5200 5500
Wire Wire Line
	5200 5450 5900 5450
$EndSCHEMATC
